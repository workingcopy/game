//
//  WordNode.swift
//  Game
//
//  Created by Anders Borum on 21/11/2017.
//  Copyright © 2017 Working Copy. All rights reserved.
//

import SpriteKit

class LetterNode : SKSpriteNode {
    
    public var offset: CGFloat = 0
    
    private var isShaking = false
    public func shake() {
        if isShaking { return }
        isShaking = true
        
        let shake = SKAction.sequence([SKAction.rotate(byAngle: CGFloat(-0.05 * Double.pi), duration: 0.1),
                                       SKAction.rotate(byAngle: CGFloat( 0.10 * Double.pi), duration: 0.2),
                                       SKAction.rotate(byAngle: CGFloat(-0.05 * Double.pi), duration: 0.1)])
        run(SKAction.repeatForever(shake), withKey: "shake")
    }
    
    public func stopShake() {
        if !isShaking { return }
        isShaking = false
        
        removeAction(forKey: "shake")
        run(SKAction.rotate(toAngle: 0, duration: 0.1))
        
    }
}

class WordNode: SKNode {
    
    var letters = [Character]()
    var sprites = [LetterNode]()
    
    public init(_ text: String) {
        super.init()
        
        var wid = CGFloat(0)
        for letter in text.lowercased() {
            if letter >= "a" && letter <= "z" {
                let child = WordNode.create(letter:letter)
                
                letters.append(letter)
                sprites.append(child)
                addChild(child)
                
                wid += child.frame.width
            }
        }
        
        var x = -0.5 * wid
        for sprite in sprites {
            sprite.position = CGPoint(x: x, y: 0)
            x += sprite.frame.width
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private static let letterScale = CGFloat(0.3)
    
    private static func create(letter: Character) -> LetterNode {
        let name = String([letter])
        let node = LetterNode(imageNamed: name)
        node.xScale = letterScale
        node.yScale = letterScale
        return node
    }
    
    // return the point of the node being removed (in parent coordinate system of WordNode)
    // or nil if nothing is removed
    public func typed(character: Character) -> CGPoint? {
        // maybe we should shake but not remove letter that match but are not first
        guard character == letters.first else { return nil }
        
        let removed = sprites[0]
        let point = convert(removed.position, to: parent!)
        
        removed.run(SKAction.fadeOut(withDuration: 0.3), completion: {
            removed.removeFromParent()
        })
        
        sprites.remove(at: 0)
        letters.remove(at: 0)
        
        return point
    }
    
    public var isDone : Bool {
        return letters.isEmpty
    }
        
    public var velocity = TimeInterval(100)
    
    public func update(deltaTime: TimeInterval) {
        guard let first = sprites.first else { return }
        let height = 0.4 * first.frame.size.height
        
        var t = CGFloat(velocity * deltaTime)
        while t > 0 {
            for sprite in sprites {
                var offset = sprite.offset
                let space = height - offset
                if space > 0 {
                    if t < space {
                        offset += t
                        t = 0
                        sprite.shake()
                    } else {
                        offset = height
                        t -= space
                        sprite.run(SKAction.moveBy(x: 0, y: -height, duration: 0.05), withKey: "move")
                        sprite.run(SKAction.playSoundFileNamed("blip.m4a", waitForCompletion: true))

                        sprite.stopShake()
                    }
                    sprite.offset = offset
                }
                
                if t <= 0.0 { break }
            }
            
            // if the last sprite is fully moved down, we move entire sprite down and children up
            let last = sprites.last!
            if last.offset >= height {
                for sprite in sprites {
                    sprite.stopShake()
                    sprite.offset = 0
                    sprite.position.y = 0
                }
                position.y -= height
                
                // move last sprite into place to cancel its last movement
                last.removeAction(forKey: "move")
                last.run(SKAction.moveBy(x: 0, y: 0, duration: 0.05), withKey: "move")
            }
        }
    }
    
    public func celebrateWin() {
        for sprite in sprites {
            sprite.shake()
            sprite.run(SKAction.scale(by: 10, duration: 20))
            
            var actions = [SKAction]()
            for s in 1...3 {
                let volume = SKAction.changeVolume(to: Float(s) * 0.2, duration: 0.5)
                let laugh = SKAction.playSoundFileNamed("alien-laugh.m4a", waitForCompletion: true)
                let delay = SKAction.wait(forDuration: 0.5 * drand48())
                actions.append(volume)
                actions.append(delay)
                actions.append(laugh)
            }
            
            sprite.run(SKAction.sequence(actions))
        }
    }
}
