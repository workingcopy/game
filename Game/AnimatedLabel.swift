import UIKit

// Label that can animate the text shown
class AnimatedLabel : UILabel {
    var displayLink: CADisplayLink?
    var startTime: CFTimeInterval?
    var targetText = String()
    
    private func stopAnimation() {
        if let existingLink = displayLink {
            existingLink.remove(from: .current, forMode: .defaultRunLoopMode)
        }
    }
    
    public func animatedSet(text: String, duration: TimeInterval) {
        stopAnimation()
        
        targetText = text
        startTime = nil
        
        let link = CADisplayLink(target: self, selector: #selector(step))
        link.add(to: .current, forMode: .defaultRunLoopMode)
        displayLink = link
    }
    
    private func adjustFontToMatchFrame() {
        let sizeOneLine: CGSize = targetText.size(withAttributes: [NSAttributedStringKey.font: font])
        
        // Get the size of the text enforcing the scaling based on label width
        let sizeOneLineConstrained: CGSize = targetText.boundingRect(with: frame.size,
                                                                     options: .usesLineFragmentOrigin,
                                                                     attributes: [NSAttributedStringKey.font: font],
                                                                     context: nil).size
        
        // Approximate scaling factor
        let approxScaleFactor: CGFloat = sizeOneLineConstrained.width / sizeOneLine.width
        
        // Approximate new point size
        let approxScaledPointSize: CGFloat = ceil(approxScaleFactor * font.pointSize)
        
        font = UIFont(name: font.fontName, size: approxScaledPointSize)
    }
    
    @objc private func step(displaylink: CADisplayLink) {
        if startTime == nil {
            startTime = displayLink?.timestamp
        }
        
        let seconds = displayLink!.timestamp - startTime!
        let expectedLength = Int(fmax(0.0, 12.0 * (seconds - 1.0)))
        
        // label should be fully drawn and we can stop animating
        if expectedLength >= targetText.count {
            text = targetText
            stopAnimation()
        } else {
            let start = targetText.startIndex
            let end = targetText.index(start, offsetBy: expectedLength)
            let substring = targetText[..<end]
            text = String(substring)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        adjustFontToMatchFrame()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        animatedSet(text: text ?? "", duration: 3.5)
        text = ""
    }
}
