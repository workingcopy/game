//
//  Laser.swift
//  Game
//
//  Created by Anders Borum on 22/11/2017.
//  Copyright © 2017 Working Copy. All rights reserved.
//

import GameKit

class Laser : SKShapeNode {
    
    class func between(_ from: CGPoint, _ to: CGPoint) -> Laser {
        let laser = Laser()
        laser.lineWidth = 3
        laser.glowWidth = 8
        laser.strokeColor = .red
        
        let path = UIBezierPath()
        path.move(to: from)
        path.addLine(to: to)
        laser.path = path.cgPath
        
        let fadeOut = SKAction.fadeOut(withDuration: 0.4)
        let sound = SKAction.playSoundFileNamed("laser.m4a", waitForCompletion: true)
        
        laser.run(SKAction.group([fadeOut, sound]), completion: {
            laser.removeFromParent()
        })

        return laser
    }
    
    class func prewarm() {
        let _ = SKAction.playSoundFileNamed("laser.m4a", waitForCompletion: true)
    }
}
