//
//  SwiftWords.swift
//  BitGame
//
//   Fetches words from Swift commit diffs from
//    https://api.github.com/repos/Apple/swift/
//   going from latest commits to older ones.
//
//  Created by Anders Borum on 15/11/2017.
//  Copyright © 2017 Working Copy. All rights reserved.
//

import Foundation

class SwiftWords {
    
    private static var sharedInstance: SwiftWords?
    public static let shared : SwiftWords = {
        if sharedInstance == nil {
            sharedInstance = SwiftWords()
        }
        return sharedInstance!
    }()
    
    static let commitsUrl = URL(string: "https://api.github.com/repos/Apple/swift/commits")!
    
    private struct CommitInfo {
        public var id : String
        public var parent : String
        
        public var diffUrl : URL {
            return URL(string: "https://api.github.com/repos/Apple/swift/compare/\(parent)...\(id)")!
        }
        
        public func readDiff(completion: (Error?, [String]) -> ()) {
            
            //URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { data, response, error in
            //}).resume()
            
        }
    }
    
    
    
}
